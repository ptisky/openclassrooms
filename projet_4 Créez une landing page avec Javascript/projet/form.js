class Form {
    //error message
    error = [
        [
            "firstname",
            false,
            "Veuillez entrer 2 caractères ou plus pour le champ",
        ],
        [
            "lastname",
            false,
            "Veuillez entrer 2 caractères ou plus pour le champ",
        ],
        ["email", false, "veuillez entrer une adresse électronique valide"],
        ["birthdate", false, "Veuillez rentrer une date uniquement."],
        ["birthdayAge", false, "Vous n'êtes pas assez agé afin de participer."],
        [
            "birthdayEmpty",
            false,
            "Vous n'avez pas entré votre date de naissance.",
        ],
        ["quantity", false, "Veuillez entrer un chiffre"],
        ["location", false, "Vous devez choisir une option."],
        [
            "checkbox",
            false,
            "Vous devez vérifier que vous acceptez les termes et conditions.",
        ],
    ];

    //check is valid form
    isvalid = false;

    constructor(
        firstname,
        lastname,
        email,
        birthdate,
        quantity,
        location,
        checkbox1,
        checkbox2
    ) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.birthdate = birthdate;
        this.quantity = quantity;
        this.location = location;
        this.checkbox1 = checkbox1;
        this.checkbox2 = checkbox2;
    }

    // METHODS //

    /**
     * Get age in int
     * @param {*} dateString
     */
    getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    /**
     * Check if its a email format ok
     * @param {*} email
     */
    emailIsValid(email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
    }

    /**
     * Check if its a date format ok
     * @param {*} date
     */
    dateIsValid(date) {
        return /^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/.test(date);
    }

    /**
     * Get Radio button value
     * @param {*} location
     */
    radio(location) {
        for (var i = 0, length = location.length; i < length; i++) {
            if (location[i].checked) {
                return location[i].value;
            }
        }
    }

    /**
     * Show error message if problem came. And check if global form is ok
     * @param {*} input
     * @param {*} bool
     * @param {*} range
     */
    displayError(input, bool, range) {
        if (input) {
            if (bool == false) {
                document.getElementById(
                    "error_" + input
                ).innerHTML = this.error[range][2];
                this.error[range][1] = false;
            } else {
                document.getElementById("error_" + input).innerHTML = " ";
                this.error[range][1] = true;
            }
        }

        if (
            this.error[0][1] &&
            this.error[1][1] &&
            this.error[2][1] &&
            this.error[3][1] &&
            this.error[4][1] &&
            this.error[5][1] &&
            this.error[6][1] &&
            this.error[7][1] &&
            this.error[8][1]
        ) {
            this.isvalid = true;
        }
    }

    /**
     * Get the information to the state of the form
     */
    validateForm() {
        if (this.isvalid == true) {
            return true;
        }
        return false;
    }

    // SETTERS //

    //firstname check length input
    set firstname(value) {
        if (value.length > 2) {
            this._firstname = value;
            console.log("first : " + this._firstname);
            this.displayError("firstname", true, 0);
        } else {
            this.displayError("firstname", false, 0);
        }
    }

    //lastname check length input
    set lastname(value) {
        if (value.length > 2) {
            this._lastname = value;
            console.log("last : " + this._lastname);
            this.displayError("lastname", true, 1);
        } else {
            this.displayError("lastname", false, 1);
        }
    }

    //email check if its valid email
    set email(value) {
        if (this.emailIsValid(value) == true) {
            this._email = value;
            console.log("email : " + this._email);
            this.displayError("email", true, 2);
        } else {
            this.displayError("email", false, 2);
        }
    }

    //birthdate check if its not empty, valid date & age > 16YO
    set birthdate(value) {
        let birthdateok;
        let birthdayAgeok;
        let birthdayEmptyok;

        //if date valid
        if (this.dateIsValid(value) == false) {
            this.displayError("birthdate", false, 3);
            birthdateok = false;
        } else {
            this.displayError("birthdate", true, 3);
            birthdateok = true;
        }

        //if age >16 YO
        if (this.getAge(value) < 16) {
            this.displayError("birthdayAge", false, 4);
            birthdayAgeok = true;
        } else {
            this.displayError("birthdayAge", true, 4);
            birthdayAgeok = false;
        }

        //if not empty
        if (value == "") {
            this.displayError("birthdayEmpty", false, 5);
            this.displayError("birthdate", true, 5);
            birthdayEmptyok = true;
        } else {
            this.displayError("birthdayEmpty", true, 5);
            birthdayEmptyok = false;
        }

        //if ok valid birthdate
        if (birthdateok && birthdayAgeok && birthdayEmptyok) {
            this._birthdate = value;
            console.log("birthdate : " + this._birthdate);
        }
    }

    //quantity check if its an integer
    set quantity(value) {
        if (
            Number.isInteger(
                parseInt(document.getElementById("quantity").value)
            )
        ) {
            this._quantity = value;
            console.log("quantity : " + this._quantity);
            this.displayError("quantity", true, 6);
        } else {
            this.displayError("quantity", false, 6);
        }
    }

    //location check if radio value exist & one is cheked
    set location(value) {
        if (
            this.radio(value) === "New York" ||
            "San Francisco" ||
            "Seattle" ||
            "Chicago" ||
            "Boston" ||
            "Portland"
        ) {
            if (this.radio(value) != undefined) {
                this._location = this.radio(value);
                console.log("location : " + this._location);
                this.displayError("location", true, 7);
            } else {
                this.displayError("location", false, 7);
            }
        } else {
            this.displayError("location", false, 7);
        }
    }

    //checkbox1 check if is cheked
    set checkbox1(value) {
        if (value == true) {
            this._checkbox1 = value;
            console.log("checkbox1 : " + this._checkbox1);
            this.displayError("checkbox", true, 8);
        } else {
            this.displayError("checkbox", false, 8);
        }
    }

    //checkbox2 only get the state of the checkbox
    set checkbox2(value) {
        this._checkbox2 = value;
        console.log("checkbox2 : " + this._checkbox2);
    }
}
