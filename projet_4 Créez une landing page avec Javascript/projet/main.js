//Get the top menu
function editNav() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

// launch modal form
function launchModal() {
    modalbg.style.display = "block";
}

// close modal form
function closeModal() {
    modalbg.style.display = "none";
}

// DOM Elements
const modalbg = document.querySelector(".bground");
const modalBtn = document.querySelectorAll(".modal-btn");
const formData = document.querySelectorAll(".formData");
const modalexit = document.querySelectorAll(".close");
const submitBtn = document.querySelectorAll(".btn-submit");

// page event
modalBtn.forEach((btn) => btn.addEventListener("click", launchModal)); //launch modal
modalexit.forEach((btn) => btn.addEventListener("click", closeModal)); //close modal
submitBtn.forEach((btn) => btn.addEventListener("click", submit)); //button submit

// submit form
function submit() {
    // new object form
    form = new Form(
        document.getElementById("first").value,
        document.getElementById("last").value,
        document.getElementById("email").value,
        document.getElementById("birthdate").value,
        document.getElementById("quantity").value,
        document.getElementsByName("location"),
        document.getElementById("checkbox1").checked,
        document.getElementById("checkbox2").checked
    );

    // check if form is ok
    if (form.validateForm() == true) {
        closeModal(); //close modal
        document.getElementById("form_validate").innerHTML =
            "Merci ! Votre réservation à été reçue.";

        console.log("Le formulaire est valide");
        console.log("*******************************");
        console.log("---------------------------------");
        console.log("*******************************");

        return true; //form ok
    } else {
        console.log("Le formulaire n'est pas valide");
        console.log("*******************************");
        console.log("---------------------------------");
        console.log("*******************************");

        return false; //form not ok
    }
}
