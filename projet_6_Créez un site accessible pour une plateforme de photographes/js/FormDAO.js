class FormDAO {
    /**
     *
     * @returns STYLES
     */
    launchModal() {
        console.log("==============================");
        console.log("Ouverture du formulaire");
        return (document.querySelector("#modal").style.display = "block");
    }

    /**
     *
     * @returns STYLES
     */
    closeModal() {
        return (document.querySelector("#modal").style.display = "none");
    }

    /**
     *
     * @param {*} event
     * @returns bool
     */
    submit(event) {
        event.preventDefault();

        // new object form
        let form = new Form(
            document.getElementById("firstname").value,
            document.getElementById("lastname").value,
            document.getElementById("email").value,
            document.getElementById("message").value
        );

        // check if form is ok
        if (form.validateForm() == true) {
            document.getElementById("form_validate").innerHTML =
                "Merci ! Votre message à été reçue.";

            console.log("Le formulaire est valide");
            console.log("*******************************");
            console.log("---------------------------------");
            console.log("*******************************");

            document.querySelector("#modal").style.display = "none";
            return true; //form ok
        } else {
            console.log("Le formulaire n'est pas valide");
            console.log("*******************************");
            console.log("---------------------------------");
            console.log("*******************************");
            return false; //form not ok
        }
    }
}
