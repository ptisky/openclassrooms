class MediaDAO extends Callback {
    constructor() {
        super();
        this.myMedia = this.myMedia.bind(this);
        this.showtags = this.showtags.bind(this);
        this.fillPhotographerPage = this.fillPhotographerPage.bind(this);
        this.showmedias = this.showmedias.bind(this);
        this.processfilter = this.processfilter.bind(this);
        this.sort = this.sort.bind(this);
    }

    /**
     *
     * @returns
     */
    myMedia() {
        let json = [];
        let medias = [];

        document.addEventListener("DOMContentLoaded", async () => {
            try {
                json = await this.loadJson();
            } catch (e) {
                console.log("Error!");
                console.log(e);
            }

            for (var b = 0; b < json.media.length; b++) {
                var obj = json.media[b];

                if (obj.video) {
                    let media = new Media(
                        obj["id"],
                        obj["photographerId"],
                        "",
                        obj["video"],
                        obj["tags"],
                        obj["likes"],
                        obj["date"],
                        obj["price"],
                        obj["span"]
                    );
                    medias.push(media);
                }

                if (obj.image) {
                    let media = new Media(
                        obj["id"],
                        obj["photographerId"],
                        obj["image"],
                        "",
                        obj["tags"],
                        obj["likes"],
                        obj["date"],
                        obj["price"],
                        obj["span"]
                    );
                    medias.push(media);
                }
            }
        });

        return medias;
    }

    /**
     *
     * @param {*} info
     * @returns
     */
    showtags(info) {
        let appendTags = "";
        for (var y = 0; y < info.tags.length; y++) {
            let tags = info.tags[y];
            appendTags +=
                `<li class="filter"><a href="./index.html#` +
                tags +
                `">#` +
                tags +
                `</a></li>`;
        }
        return appendTags;
    }

    /**
     *
     * @param {*} photographers
     * @param {*} medias
     */
    fillPhotographerPage(photographers, medias) {
        let appendHeader = "";
        let appendWall = "";
        let appendModal = "";
        let likes = 0;
        let price = 0;

        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const url_id = urlParams.get("id");

        setTimeout(() => {
            //get section informations
            for (var i = 0; i < photographers.length; i++) {
                let id_photographer = photographers[i].id;
                let info = photographers[i];
                price = info.price;

                if (url_id == id_photographer) {
                    // Get tags
                    appendWall = this.showmedias(medias, id_photographer, info)
                        .medias;
                    likes = this.showmedias(medias, id_photographer, info)
                        .likes;

                    appendHeader += `
                    <h1 class="photographer_title">${info.name}</h1>
                    <h2>${info.city}, ${info.country}</h2>
                    <p>${info.tagline}</p>
                    <ul>${this.showtags(info)}</ul>
                    <button class="open_modal">Contactez-moi</button>
                    <em id="form_validate"></em>
                    <img src="img/Photographers/${
                        info.portrait
                    }" alt="Photo de ${info.name}" />
                `;

                    appendModal += info.name;
                }
            }

            var p = window.location.pathname;
            if (p.match(/^\/?photographer/)) {
                //append elements in section
                document.getElementById(
                    "header_photographer"
                ).innerHTML = appendHeader;
                document.getElementById("wall").innerHTML = appendWall;
                document.getElementById(
                    "modal_form_name"
                ).innerHTML = appendModal;
                document.getElementById("float-like").innerHTML = likes + " ❤";
                document.getElementById("float-price").innerHTML =
                    price + " € / jour";
            }
        }, 100);
    }

    /**
     *
     * @param {*} medias
     * @param {*} id_photographer
     * @param {*} info
     * @returns
     */
    showmedias(medias, id_photographer, info) {
        let appendWall = "";
        let appendLightBox = "";
        let likes = 0;
        for (var a = 0; a < medias.length; a++) {
            let media = medias[a];
            if (id_photographer == media.photographerId) {
                likes = likes + media.likes;
                if (media.video != "") {
                    appendWall += `
                        <section>
                            <a href="#" class="show_media">
                                <video autoplay muted id="${media.id}" class="getmedia">
                                    <source src="img/${info.name}/${media.video}" type="video/mp4">
                                </video>
                            </a>
                            <div class="content">
                                <h3>${media.span}</h3>
                                <span>${media.price}€</span>
                                <em class="fas fa-heart likes_media">${media.likes}</em>
                            </div>
                        </section>
                     `;

                    appendLightBox += `
                 <div class="item ${media.id}">
                     <section class="container">
                     <video autoplay muted id="${media.id}" class="getmedia">
                         <source src="img/${info.name}/${media.video}" type="video/mp4">
                     </video>
                         <p class="author">${media.span}</p>
                     </section>
                 </div>
              `;
                }

                if (media.video == "") {
                    appendWall += `
                    <section>
                        <a href="#" class="show_media">
                            <img src="img/${info.name}/${media.image}" id="${media.id}" class="getmedia" alt="${media.span}" />
                        </a>
                            <div class="content">
                                <h3>${media.span}</h3>
                                <span>${media.price}€</span>
                                <em class="fas fa-heart likes_media">${media.likes}</em>
                            </div>
                    </section>
                    `;

                    appendLightBox += `
                    <div class="item ${media.id}">
                        <section class="container">
                            <img src="img/${info.name}/${media.image}" id="${media.id}" class="getmedia" alt="${media.span}" />
                            <p class="author">${media.span}</p>
                        </section>
                    </div>
                 `;
                }
            }
        }

        const appendLightBoxcontainer = document.querySelectorAll(
            ".carousel-inner"
        )[0];

        appendLightBoxcontainer.innerHTML = appendLightBox;

        return {
            medias: appendWall,
            likes: likes,
        };
    }

    /**
     *
     * @param {*} filter_array
     * @param {*} photographers
     */
    processfilter(filter_array, photographers) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const url_id = urlParams.get("id");

        for (var i = 0; i < photographers.length; i++) {
            if (photographers[i].id == url_id) {
                document.getElementById("wall").innerHTML = this.showmedias(
                    filter_array,
                    url_id,
                    photographers[i]
                ).medias;
            }
        }
        // -------- INCREASE LIKES ------- //
        //close modal
        const clickLike = document.querySelectorAll(".likes_media");
        clickLike.forEach((btn) =>
            btn.addEventListener("click", MedDAO.incrementLike)
        );
        // -------- .INCREASE LIKES ------- //

        //open lightbox
        const show_media = document.querySelectorAll(".show_media");
        show_media.forEach((btn) =>
            btn.addEventListener(
                "click",
                function () {
                    ModDAO = new ModalDAO();
                    ModDAO.launchModalMedia(this);
                },
                false
            )
        );
    }

    /**
     *
     * @param {*} photographers
     * @param {*} medias
     */
    sort(photographers, medias) {
        const button_choice = document.getElementById("photo-select").value;

        if (button_choice == "date") {
            const sort_date = medias.sort((a, b) => {
                return new Date(a.date).valueOf() - new Date(b.date).valueOf();
            });
            this.processfilter(sort_date, photographers);
        } else if (button_choice == "titre") {
            const sort_name = medias.sort((a, b) => {
                if (a.span.toLowerCase() < b.span.toLowerCase()) {
                    return -1;
                } else if (a.span.toLowerCase() > b.span.toLowerCase()) {
                    return 1;
                }
            });
            this.processfilter(sort_name, photographers);
        } else if (button_choice == "fame") {
            //filtre sur fame
            const sort_fame = medias.sort((a, b) => {
                return b.likes - a.likes;
            });
            this.processfilter(sort_fame, photographers);
        }
    }

    /**
     * Increment Likes
     */
    incrementLike() {
        const currentlike = this.innerHTML;
        if (!this.classList.contains("liked")) {
            const getTotal = document.querySelectorAll("#float-like")[0];
            const currentTotalLike = getTotal.innerHTML;
            const currentTotalLikeSplited = currentTotalLike.split("❤");
            const currenttrueTotal = currentTotalLikeSplited[0];

            let likeObjectInc = parseInt(currentlike, 10) + 1;
            let likeTotalInc = parseInt(currenttrueTotal, 10) + 1;

            this.innerHTML = likeObjectInc;
            getTotal.innerHTML = likeTotalInc + " ❤";
            this.classList.add("liked");
        }
    }
}
