class ModalDAO extends Callback {
    /**
     *
     * @param {*} medias
     * @param {*} elem
     */
    launchModalMedia(elem) {
        let idmedia = elem.children[0].getAttribute("id");
        const appendLightBoxcontainer = document.querySelectorAll(".item");
        let indic = "";
        window.currentItem = 0;
        window.isEnabled = true;

        for (var i = 0; i < appendLightBoxcontainer.length; i++) {
            let idLb = appendLightBoxcontainer[i].className.split("item")[1];
            if (appendLightBoxcontainer[i].classList.contains("active")) {
                appendLightBoxcontainer[i].classList.remove("active");
            }

            if (parseInt(idmedia, 10) == parseInt(idLb, 10)) {
                window.currentItem = i;
                appendLightBoxcontainer[i].classList.add("active");
            }

            indic += "<li></li>";
        }

        document.querySelectorAll(".carousel-indicators")[0].innerHTML = indic;
        document.querySelector("#media-nav").style.display = "flex";

        let dots = document.querySelectorAll(".carousel-indicators li");
        dots[window.currentItem].classList.add("active");
    }

    /**
     *
     * @returns styles
     */
    closeModalMedia() {
        window.currentItem = 0;
        window.isEnabled = false;
        let items = document.querySelectorAll("#media-nav .item");
        let dots = document.querySelectorAll(".carousel-indicators li");
        items[window.currentItem].classList.remove("active");
        dots[window.currentItem].classList.remove("active");
        return (document.querySelector("#media-nav").style.display = "none");
    }

    /**
     *
     * @param {*} n
     */
    changeCurrentItem(n) {
        let items = document.querySelectorAll("#media-nav .item");
        window.currentItem = (n + items.length) % items.length;
    }

    /**
     *
     * @param {*} n
     */
    nextItem(n) {
        this.hideItem("to-left");
        this.changeCurrentItem(n + 1);
        this.showItem("from-right");
    }

    /**
     *
     * @param {*} n
     */
    previousItem(n) {
        this.hideItem("to-right");
        this.changeCurrentItem(n - 1);
        this.showItem("from-left");
    }

    /**
     *
     * @param {*} n
     */
    goToItem(n) {
        if (n < window.currentItem) {
            this.hideItem("to-right");
            window.currentItem = n;
            this.showItem("from-left");
        } else {
            hideItem("to-left");
            window.currentItem = n;
            this.showItem("from-right");
        }
    }

    /**
     *
     * @param {*} direction
     */
    hideItem(direction) {
        let items = document.querySelectorAll("#media-nav .item");
        let dots = document.querySelectorAll(".carousel-indicators li");
        window.isEnabled = false;
        items[window.currentItem].classList.add(direction);
        dots[window.currentItem].classList.remove("active");
        items[window.currentItem].addEventListener("animationend", function () {
            this.classList.remove("active", direction);
        });
    }

    /**
     *
     * @param {*} direction
     */
    showItem(direction) {
        let items = document.querySelectorAll("#media-nav .item");
        let dots = document.querySelectorAll(".carousel-indicators li");
        items[window.currentItem].classList.add("next", direction);
        dots[window.currentItem].classList.add("active");
        items[window.currentItem].addEventListener("animationend", function () {
            this.classList.remove("next", direction);
            this.classList.add("active");
            window.isEnabled = true;
        });
    }
}
