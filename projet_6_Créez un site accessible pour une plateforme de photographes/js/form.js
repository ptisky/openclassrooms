class Form {
    //error message
    error = [
        [
            "firstname",
            false,
            "Veuillez entrer 2 caractères ou plus pour le champ",
        ],
        [
            "lastname",
            false,
            "Veuillez entrer 2 caractères ou plus pour le champ",
        ],
        ["email", false, "veuillez entrer une adresse électronique valide"],
        [
            "message",
            false,
            "Veuillez entrer 2 caractères ou plus pour le champ",
        ],
    ];

    //check is valid form
    isvalid = false;

    /**
     *
     * @param {*} firstname
     * @param {*} lastname
     * @param {*} email
     * @param {*} message
     */
    constructor(firstname, lastname, email, message) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.message = message;
    }

    // METHODS //

    /**
     * Check if its a email format ok
     * @param {*} email
     */ j;
    emailIsValid(email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
    }

    /**
     * Show error message if problem came. And check if global form is ok
     * @param {*} input
     * @param {*} bool
     * @param {*} range
     */
    displayError(input, bool, range) {
        if (input) {
            if (bool == false) {
                document.getElementById(
                    "error_" + input
                ).innerHTML = this.error[range][2];
                this.error[range][1] = false;
            } else {
                document.getElementById("error_" + input).innerHTML = " ";
                this.error[range][1] = true;
            }
        }

        if (
            this.error[0][1] &&
            this.error[1][1] &&
            this.error[2][1] &&
            this.error[3][1]
        ) {
            this.isvalid = true;
        }
    }

    /**
     * Get the information to the state of the form
     */
    validateForm() {
        if (this.isvalid == true) {
            return true;
        }
        return false;
    }

    // SETTERS //

    //firstname check length input
    set firstname(value) {
        if (value.length >= 2) {
            this._firstname = value;
            console.log("firstname : " + this._firstname);
            this.displayError("firstname", true, 0);
        } else {
            this.displayError("firstname", false, 0);
        }
    }

    //lastname check length input
    set lastname(value) {
        if (value.length >= 2) {
            this._lastname = value;
            console.log("lastname : " + this._lastname);
            this.displayError("lastname", true, 1);
        } else {
            this.displayError("lastname", false, 1);
        }
    }

    //email check if its valid email
    set email(value) {
        if (this.emailIsValid(value) == true) {
            this._email = value;
            console.log("email : " + this._email);
            this.displayError("email", true, 2);
        } else {
            this.displayError("email", false, 2);
        }
    }

    //lastname check length input
    set message(value) {
        if (value.length >= 2) {
            this._message = value;
            console.log("message : " + this._message);
            this.displayError("message", true, 3);
        } else {
            this.displayError("message", false, 3);
        }
    }
}
