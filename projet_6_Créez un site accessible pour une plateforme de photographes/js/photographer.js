class Photographer {
    /**
     *
     * @param {*} name
     * @param {*} id
     * @param {*} city
     * @param {*} country
     * @param {*} tagline
     * @param {*} price
     * @param {*} tags
     * @param {*} portrait
     * @param {*} span
     */
    constructor(name, id, city, country, tagline, price, tags, portrait, span) {
        this.name = name;
        this.id = id;
        this.city = city;
        this.country = country;
        this.tagline = tagline;
        this.price = price;
        this.tags = tags;
        this.portrait = portrait;
    }

    set name(value) {
        this._name = value;
    }

    set id(value) {
        this._id = value;
    }

    set city(value) {
        this._city = value;
    }

    set country(value) {
        this._country = value;
    }

    set tagline(value) {
        this._tagline = value;
    }

    set price(value) {
        this._price = value;
    }

    set tags(value) {
        this._tags = value;
    }

    set portrait(value) {
        this._portrait = value;
    }

    get name() {
        return this._name;
    }

    get id() {
        return this._id;
    }

    get city() {
        return this._city;
    }

    get country() {
        return this._country;
    }

    get tagline() {
        return this._tagline;
    }

    get price() {
        return this._price;
    }

    get tags() {
        return this._tags;
    }

    get portrait() {
        return this._portrait;
    }
}
