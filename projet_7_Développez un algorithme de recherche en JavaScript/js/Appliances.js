class Appliances extends Filter {
    fillAppliances(appliances) {
        const list_appliances = [];

        appliances.forEach((appliance) => {
            if (list_appliances.indexOf(appliance) == -1) {
                list_appliances.push(appliance);
                AllAppliances.push(appliance);
                document.querySelector(
                    ".options_container.secondary"
                ).innerHTML += `<li class="option" data-value="${appliance}">${appliance}</li>`;
            }
        });

        this.fillFilter("secondary");
    }

    fillAppliancesSearch(appliances) {
        appliances.forEach((appliance) => {
            document.querySelector(
                ".options_container.secondary"
            ).innerHTML += `<li class="option" data-value="${appliance}">${appliance}</li>`;
        });
        this.fillFilter("secondary");
    }
}
