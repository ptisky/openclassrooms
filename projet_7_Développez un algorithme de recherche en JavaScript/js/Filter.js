class Filter {
    fillPlates(Allrecipes) {
        Allrecipes.forEach((recipe) => {
            let list_ingredient = "";
            recipe.ingredients.forEach((element) => {
                const ingredient = element.ingredient;
                let quantity = "";

                if (element.unit != undefined) {
                    quantity = element.quantity + " " + element.unit;
                } else if (element.quantity != undefined) {
                    quantity = element.quantity;
                } else {
                    quantity = "";
                }

                list_ingredient += `<p><b>${ingredient}:</b> <span>${quantity}</span></p>`;
            });

            document.querySelector("#recipe").innerHTML += `
                    <section class="card_recipe">
                        <img src="https://via.placeholder.com/600x400" alt="" />
                        <div class="recipe_desc_container">
                            <h2>${recipe.name}</h2>
                            <em><img src="img/watch.svg"> ${recipe.time}min</em>
                            <div class="list_recipe">${list_ingredient}</div>
                            <div class="line-clamp">
                                <p class="desc_recipe">${recipe.description}</p>
                            </div>
                        </div>
                    </section>
                `;
        });
    }

    addFilter(filter, category_filter) {
        if (category_filter == "primary") {
            AllIngredientsFilter.push(filter.getAttribute("data-value"));
        }

        if (category_filter == "secondary") {
            AllAppliancesFilter.push(filter.getAttribute("data-value"));
        }

        if (category_filter == "tiertary") {
            AllUstensilsFilter.push(filter.getAttribute("data-value"));
        }

        this.FilterByIngredient(AllIngredientsFilter);
        this.FilterByAppareil(AllAppliancesFilter);
        this.FilterByUstensiles(AllUstensilsFilter);

        if (!filter.classList.contains("selected")) {
            filter.classList.add("selected");
            document.querySelector(
                "#filter_added"
            ).innerHTML += `<li class="filter_selected ${category_filter}">${filter.getAttribute(
                "data-value"
            )}<img src="img/cross.svg" class="remove_filter"></li>`;
        }

        const options_remove = document.querySelectorAll(".remove_filter");
        for (let i = 0; i < options_remove.length; i++) {
            options_remove[i].addEventListener("click", function () {
                FilterDAO.removeFilter(this);
            });
        }
    }

    fillFilter(type) {
        let options = document.querySelectorAll(".options_container .option");
        for (let i = 0; i < options.length; i++) {
            if (type == "primary") {
                this.FilterByIngredient(options[i].getAttribute("data-value"));
            }

            if (type == "secondary") {
                this.FilterByAppareil(options[i].getAttribute("data-value"));
            }

            if (type == "tiertary") {
                this.FilterByUstensiles(options[i].getAttribute("data-value"));
            }
        }
    }

    removeFilter(filter) {
        filter.parentNode.remove();

        const currentLi = filter.parentNode.innerHTML.split(
            '<img src="img/cross.svg" class="remove_filter">'
        );

        const currentcat = filter.parentNode.classList[1];

        if (currentcat == "primary") {
            for (var i = 0; i < AllIngredientsFilter.length; i++) {
                if (AllIngredientsFilter[i] == currentLi[0]) {
                    AllIngredientsFilter.splice(i, 1);
                }
            }
        }

        if (currentcat == "secondary") {
            for (var i = 0; i < AllAppliancesFilter.length; i++) {
                if (AllAppliancesFilter[i] == currentLi[0]) {
                    AllAppliancesFilter.splice(i, 1);
                }
            }
        }

        if (currentcat == "tiertary") {
            for (var i = 0; i < AllUstensilsFilter.length; i++) {
                if (AllUstensilsFilter[i] == currentLi[0]) {
                    AllUstensilsFilter.splice(i, 1);
                }
            }
        }

        if (AllIngredientsFilter.length) {
            this.FilterByIngredient(AllIngredientsFilter);
        }

        if (AllAppliancesFilter.length) {
            this.FilterByAppareil(AllAppliancesFilter);
        }

        if (AllUstensilsFilter.length) {
            this.FilterByUstensiles(AllUstensilsFilter);
        }

        if (
            !AllIngredientsFilter.length &&
            !AllAppliancesFilter.length &&
            !AllUstensilsFilter.length
        ) {
            this.fillPlates(Allrecipes);
        }

        let options = document.querySelectorAll(".options_container li");
        for (let i = 0; i < options.length; i++) {
            if (options[i].getAttribute("data-value") == currentLi[0]) {
                options[i].classList.remove("selected");
            }
        }
    }

    FilterByIngredient(filter) {
        if (AllIngredientsFilter != "") {
            const returnPlates = [];
            document.querySelector("#recipe").innerHTML = ``;

            for (let index = 0; index < filter.length; index++) {
                Allrecipes.forEach((recipe) => {
                    recipe.ingredients.forEach((element) => {
                        const ingredient = element.ingredient;
                        if (ingredient == filter[index]) {
                            returnPlates.push(recipe);
                        }
                    });
                });
            }
            this.fillPlates(returnPlates);
        }
    }

    FilterByAppareil(filter) {
        if (AllAppliancesFilter != "") {
            const returnPlates = [];
            document.querySelector("#recipe").innerHTML = ``;

            for (let index = 0; index < filter.length; index++) {
                const filtered = Allrecipes.filter((recipe) => {
                    return recipe.description
                        .toLowerCase()
                        .includes(filter[index].toLowerCase());
                });

                filtered.forEach((element) => {
                    returnPlates.push(element);
                });
            }

            this.fillPlates(returnPlates);
        }
    }

    FilterByUstensiles(filter) {
        if (AllUstensilsFilter != "") {
            const returnPlates = [];
            document.querySelector("#recipe").innerHTML = ``;

            for (let index = 0; index < filter.length; index++) {
                const filtered = Allrecipes.filter((recipe) => {
                    return recipe.description
                        .toLowerCase()
                        .includes(filter[index].toLowerCase());
                });

                filtered.forEach((element) => {
                    returnPlates.push(element);
                });
            }

            this.fillPlates(returnPlates);
        }
    }

    clickOption(element) {
        let val = element.getAttribute("data-value");
        let drop = element.parentElement;
        const prev =
            element.parentElement.childNodes[3].classList.contains("active");

        let prevActive = null;
        if (prev === true) {
            prevActive =
                element.parentElement.childNodes[3].getAttribute("data-value");
        }
        let options = document.querySelector(".drop .option").length;

        drop.querySelectorAll(".option.active")[0].classList.add("mini-hack");
        drop.classList.toggle("visible");
        drop.classList.toggle("withBG");
        element.style.top;
        drop.classList.toggle("opacity");

        document.querySelector(".mini-hack").classList.remove("mini-hack");

        if (drop.classList.contains("visible")) {
            setTimeout(function () {
                drop.classList.add("withBG");
            }, 400 + options * 100);
        }

        this.triggerAnimation(drop);
        if (val !== "placeholder" || prevActive === "placeholder") {
            document.querySelector(".option_click").classList.remove("active");
            element.classList.add("active");
        }
    }

    triggerAnimation(drop) {
        let finalWidth = drop.classList.contains("visible") ? 25 : 10;
        drop.style.width = "10em";
        setTimeout(function () {
            drop.style.width = finalWidth + "em";
        }, 400);
    }

    showSuggestionMessage(filtered) {
        if (filtered.length <= 0) {
            let pElement = document.createElement("p");
            container.innerHTML = "";
            pElement.innerHTML =
                "Aucune recette ne correspond à votre critère… vous pouvez chercher « tarte aux pommes », « poisson », etc.";
            container.append(pElement);
        } else {
            container.innerHTML = "";
            this.fillPlates(filtered);
        }
    }
}
