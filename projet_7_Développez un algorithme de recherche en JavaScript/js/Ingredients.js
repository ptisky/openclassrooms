class Ingredients extends Filter {
    fillIngredients(ingredients) {
        const list_ingredients = [];

        ingredients.forEach((list_ingredient) => {
            list_ingredient.forEach((ingredient) => {
                if (list_ingredients.indexOf(ingredient.ingredient) == -1) {
                    list_ingredients.push(ingredient.ingredient);
                    AllIngredients.push(ingredient.ingredient);
                    document.querySelector(
                        ".options_container.primary"
                    ).innerHTML += `<li class="option" data-value="${ingredient.ingredient}">${ingredient.ingredient}</li>`;
                }
            });
        });

        this.fillFilter("primary");
    }

    fillIngredientsSearch(ingredients) {
        ingredients.forEach((ingredient) => {
            document.querySelector(
                ".options_container.primary"
            ).innerHTML += `<li class="option" data-value="${ingredient}">${ingredient}</li>`;
        });
        this.fillFilter("primary");
    }
}
