class Recipes {
    constructor(
        id,
        name,
        servings,
        time,
        description,
        appliance,
        ustensils,
        ingredients
    ) {
        this.id = id;
        this.name = name;
        this.servings = servings;
        this.time = time;
        this.description = description;
        this.appliance = appliance;
        this.ustensils = ustensils;
        this.ingredients = ingredients;
    }

    set id(value) {
        this._id = value;
    }

    set name(value) {
        this._name = value;
    }

    set servings(value) {
        this._servings = value;
    }

    set time(value) {
        this._time = value;
    }

    set description(value) {
        this._description = value;
    }

    set appliance(value) {
        this._appliance = value;
    }

    set ustensils(value) {
        this._ustensils = value;
    }

    set ingredients(value) {
        this._ingredients = value;
    }

    get id() {
        return this._id;
    }

    get name() {
        return this._name;
    }

    get servings() {
        return this._servings;
    }

    get time() {
        return this._time;
    }

    get description() {
        return this._description;
    }

    get appliance() {
        return this._appliance;
    }

    get ustensils() {
        return this._ustensils;
    }

    get ingredients() {
        return this._ingredients;
    }
}
