class Ustensils extends Filter {
    fillUtensils(ustensils) {
        const list_ustensils = [];

        ustensils.forEach((all_ustensils) => {
            all_ustensils.forEach((ustensils) => {
                if (list_ustensils.indexOf(ustensils) == -1) {
                    list_ustensils.push(ustensils);
                    AllUstensils.push(ustensils);
                    document.querySelector(
                        ".options_container.tiertary"
                    ).innerHTML += `<li class="option" data-value="${ustensils}">${ustensils}</li>`;
                }
            });
        });

        this.fillFilter("tiertary");
    }

    fillUtensilsSearch(ustensils) {
        ustensils.forEach((ustensil) => {
            document.querySelector(
                ".options_container.tiertary"
            ).innerHTML += `<li class="option" data-value="${ustensil}">${ustensil}</li>`;
        });
        this.fillFilter("tiertary");
    }
}
